# BLOG APPLICATION #

This web application manages the posts and comments associated with them via Ruby on Rails in a POSTGRES database. 

### Details ###

* Create/Edit posts
* Create/Edit comments
* Deleting posts will delete all its comments
* Using basic authentication to allow editing of posts and comments